# nextjs-hello-world

A simple Hello World project created with Nextjs.

## Dependencies

This demo requires `node` and `npm` to be installed locally.
See the [.nvmrc](nextjs-hello-world-app/.nvmrc) file for the correct node version.

To install all further dependencies, run

```bash
npm --prefix ./nextjs-hello-world-app install
```

## Start

To start the application, run

```bash
npm --prefix ./nextjs-hello-world-app run dev
```

## Features

###  Pages

Go to http://localhost:3000/ to see the Hello World page.

### Components

The Hello World message is encapsulated in the Hello component.

### API Routes

Nextjs comes with a backend built-in.

```bash
curl -X GET http://localhost:3000/api/hello
```