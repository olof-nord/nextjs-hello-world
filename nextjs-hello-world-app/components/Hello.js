export default function Hello({ message }) {
  return (
    <div className="hello-component">
        <h1>
          Hello { message }!
        </h1>
    </div>
  )
}
