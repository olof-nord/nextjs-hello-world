import Head from 'next/head'

import Hello from '../components/Hello.js'

function HelloPage({ data }) {
  return (
    <div className="hello-page">
      <Head>
        <title>Hello World</title>
      </Head>
      <main>
        <Hello message={ data.hello }/>
      </main>
    </div>
  )
}

// This gets called on every request
export async function getServerSideProps() {
  // Fetch data from its own backend
  const URL = "http://localhost:3000/api/hello"
  const res = await fetch(URL)
  const data = await res.json()

  // Pass data to the page via props
  return { props: { data } }
}

export default HelloPage